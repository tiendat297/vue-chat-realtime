import Vue from 'vue'
import App from './App.vue'
import firebase from 'firebase';

Vue.config.productionTip = false

const firebaseConfig = {
  apiKey: "AIzaSyBAuEpiDwl7Sfchab7Ikc6G_FxjLYGYlW4",
  authDomain: "my-vue-chat-c4783.firebaseapp.com",
  projectId: "my-vue-chat-c4783",
  storageBucket: "my-vue-chat-c4783.appspot.com",
  messagingSenderId: "289007736065",
  appId: "1:289007736065:web:fe29e2bcdf173e7d313700"
};

firebase.initializeApp(firebaseConfig)

firebase.auth().onAuthStateChanged(() => new Vue({
  render: h => h(App),
}).$mount('#app'))
